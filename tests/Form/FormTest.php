<?php

use Form\FormFactory;

require_once __DIR__ . '/Demo/DemoForm.php';

class FormTest extends PHPUnit_Framework_TestCase
{
    protected $form;

    public function setUp()
    {
        $formObject = new DemoForm();

        $formFactory = FormFactory::getInstance()->getFactory();

        $form = $formFactory->createBuilder($formObject, $formObject->getDefaultData(), $formObject->getOptions());

        $this->form = $form->getForm();
    }

    public function testFormType()
    {
        $this->assertTrue($this->form instanceof \Symfony\Component\Form\Form);
    }

}
