<?php

use Form\AbstractForm;

class DemoForm extends AbstractForm
{
    /**
     * <code>
     * $this
     *      ->addText('username', $options)
     *      ->addPassword('password')
     *      ->addEmail('email')
     *      ... etc ...
     * </code>
     *
     * Do not include buttons inside form. Submit buttons should be in views
     *
     * @param array $options
     *
     * @return mixed
     */
    public function createForm(array $options)
    {
        $this
            ->addText('username')
            ->addEmail('email')
        ;
    }


    /**
     * @return array
     */
    public function getDefaultData()
    {
        return [];
    }

}
