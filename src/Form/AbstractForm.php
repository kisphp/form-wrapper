<?php

namespace Form;

use Form\Exceptions\FormFieldNameException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

abstract class AbstractForm extends AbstractType
{
    const PREFIX = 'add';

    /**
     * @var FormBuilderInterface
     */
    protected $builder;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;
        $this->createForm($options);
    }

    /**
     * <code>
     * $this
     *      ->addText('username', $options)
     *      ->addPassword('password')
     *      ->addEmail('email')
     *      ... etc ...
     * </code>
     *
     * Do not include buttons inside form. Submit buttons should be in views
     *
     * @param array $options
     *
     * @return mixed
     */
    public abstract function createForm(array $options);

    /**
     * @return array
     */
    public abstract function getDefaultData();

    /**
     * @param string $name
     * @param null $type
     * @param array $options
     *
     * @return AbstractForm
     */
    public function add($name, $type=null, array $options=[])
    {
        $this->builder->add($name, $type, $options);

        return $this;
    }

    /**
     * @param array $parameters
     *
     * @throws FormFieldNameException
     * @return array
     */
    private function getFormFieldsParameters(array $parameters)
    {
        if (empty($parameters[0])) {
            throw new FormFieldNameException();
        }

        $fieldName = $parameters[0];
        $options = (isset($parameters[1]) && !empty($parameters[1])) ? $parameters[1] : [];

        return [
            $fieldName,
            $options,
        ];
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [];
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'form';
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addText($name, array $options=[])
    {
        return $this->add($name, 'text', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addTextarea($name, array $options=[])
    {
        return $this->add($name, 'textarea', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addEmail($name, array $options=[])
    {
        return $this->add($name, 'email', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addCheckbox($name, array $options=[])
    {
        return $this->add($name, 'checkbox', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addInteger($name, array $options=[])
    {
        return $this->add($name, 'integer', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addMoney($name, array $options=[])
    {
        return $this->add($name, 'money', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addPassword($name, array $options=[])
    {
        return $this->add($name, 'password', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addPercent($name, array $options=[])
    {
        return $this->add($name, 'percent', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addSearch($name, $options = [])
    {
        return $this->add($name, 'search', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addUrl($name, $options = [])
    {
        return $this->add($name, 'url', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addChoice($name, $options = [])
    {
        return $this->add($name, 'choice', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addEntity($name, $options = [])
    {
        return $this->add($name, 'entity', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addCountry($name, $options = [])
    {
        return $this->add($name, 'country', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addLanguage($name, $options = [])
    {
        return $this->add($name, 'language', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addLocale($name, $options = [])
    {
        return $this->add($name, 'locale', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addTimezone($name, $options = [])
    {
        return $this->add($name, 'timezone', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addCurrency($name, $options = [])
    {
        return $this->add($name, 'currency', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addDate($name, $options = [])
    {
        return $this->add($name, 'date', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addDatetime($name, $options = [])
    {
        return $this->add($name, 'datetime', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addTime($name, $options = [])
    {
        return $this->add($name, 'time', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addBirthday($name, $options = [])
    {
        return $this->add($name, 'birthday', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addFile($name, $options = [])
    {
        return $this->add($name, 'file', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addRadio($name, $options = [])
    {
        return $this->add($name, 'radio', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addCollection($name, $options = [])
    {
        return $this->add($name, 'collection', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addRepeated($name, $options = [])
    {
        return $this->add($name, 'repeated', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addHidden($name, $options = [])
    {
        return $this->add($name, 'hidden', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addReset($name, $options = [])
    {
        return $this->add($name, 'reset', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addForm($name, $options = [])
    {
        return $this->add($name, 'form', $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addSelect($name, $options = [])
    {
        return $this->add($name, new SelectType(), $options);
    }

    /**
     * @param string $name
     * @param array $options
     *
     * @return AbstractForm
     */
    public function addAutosuggest($name, $options = [])
    {
        return $this->add($name, new AutosuggestType(), $options);
    }
}
