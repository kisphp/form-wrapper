<?php

namespace Form;

use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormExtensionInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Form\FormFactoryInterface;

class FormFactory
{
    private static $instance;

    private $factory;

    /**
     * @return FormFactory
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->factory = \Symfony\Component\Form\Forms::createFormFactoryBuilder();
        $this->addExtension(new HttpFoundationExtension());
        $validator = Validation::createValidator();
        $this->addExtension(new ValidatorExtension($validator));
    }

    /**
     * @param FormExtensionInterface $extension
     *
     * @return FormFactory
     */
    public function addExtension(FormExtensionInterface $extension)
    {
        $this->factory->addExtension($extension);

        return $this;
    }

    /**
     * @return FormFactoryInterface
     */
    public function getFactory()
    {
        return $this->factory->getFormFactory();
    }
}
